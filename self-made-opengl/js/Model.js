/*
Creation date : 17/11/2017
Author : Sytrix
*/

function Model(gl) {
	this.gl = gl;
	this.vertexObject = null;
	this.shader = null;
	this.texture = null;
}

// Setter
Model.prototype.setShader = function(shader) {
	this.shader = shader;
}

Model.prototype.setTexture = function(texture) {
	this.texture = texture;
}

Model.prototype.setVertexObject = function(vertexObject) {
	this.vertexObject = vertexObject;
}

// draw the model
Model.prototype.draw = function(pMatrix, mvMatrix) {
	if(this.vertexObject == null) {
		return;
	}
	if(this.shader == null) {
		return;
	}

	this.vertexObject.draw(pMatrix, mvMatrix, this.shader, this.texture);
}