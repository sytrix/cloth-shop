/*
Creation date : 25/11/2017
Author : Sytrix
*/

function VertexObject(gl, url) {
	this.gl = gl;
	this.parts = [];
    this.loadFromUrl(url);
}

// Request OBJ file 
VertexObject.prototype.loadFromUrl = function(url) {
	var httpRequest = false;

    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    var thisIsMe = this;
    httpRequest.onreadystatechange = function() { thisIsMe.receive(httpRequest); };
    httpRequest.open('GET', url, true);
    httpRequest.send(null);
}

// Receive previous request
VertexObject.prototype.receive = function(httpRequest) {
	if (httpRequest.readyState == XMLHttpRequest.DONE) {
	    if (httpRequest.status == 200) {
	    	this.receiveOk(httpRequest.responseText);
	    } else {
	        alert('Un problème est survenu avec la requête.');
	    }
	}
}

// If status code == 200 -> we can load the content into buffer
VertexObject.prototype.receiveOk = function(fileContent) {
	var listObj = [];
	var objVertexPositionBuffer = [];
	var objVertexTextureCoordBuffer = [];
	var objVertexNormal = [];
	var objVertexIndexBuffer = [];
    var lines = fileContent.split(/\n/);
    var groupDef = false;
    var groupName = '';
    var listVertex = [];
    var listTextureCoord = [];
	var listNormal = [];
    var indexFace = 0;
    var nbBlock = 0;
    var blockBeginIndex = 0;
    for(var i = 0; i < lines.length; i++) {
    	var line = lines[i];
    	if(!line.startsWith('#')) {
        	var params = line.split(/[ \r\n]/).filter(function(el) {return (el.length > 0);});
        	if(params.length > 0) {
        		var cmd = params[0];
    			if(cmd == 'v') {
    				if(groupDef) {
        				listObj.push({
        					'name':groupName,
        					'vPosition':objVertexPositionBuffer,
        					'vTextureCoord':objVertexTextureCoordBuffer,
							'vNormal':objVertexNoraml,
        					'vIndex':objVertexIndexBuffer,
        					'ready':false,
        				});

        				groupDef = false;
        				groupName = '';
        				indexFace = 0;
        				nbBlock = 0;
						blockBeginIndex = 0;

        				objVertexPositionBuffer = [];
        				objVertexTextureCoordBuffer = [];
						objVertexNormal = [];
        				objVertexIndexBuffer = [];
    				}
    				if(params.length == 4) {
    					listVertex.push([params[1], params[2], params[3]]);
    				}
    			} else if(cmd == 'vt') {
    				if(params.length > 2) {
        				listTextureCoord.push([params[1], params[2]]);
        			}
    			} else if(cmd == 'vn') {
					if(params.length == 4) {
						listNormal.push([params[1], params[2], params[3]]);
					}
				} else if(cmd == 'g') {
					if(params.length == 2) {
						groupDef = true;
        				groupName = params[1];
        			}
    			} else if(cmd == 'f') {
    				var nbVertex = params.length - 1;
    				if(nbVertex >= 3) {
        				var faceVertex = [];
        				var indexBlock = Math.floor((indexFace + nbVertex) / 65536);
        				if(indexBlock == nbBlock) {
        					nbBlock = indexBlock + 1; 
        					objVertexPositionBuffer[indexBlock] = [];
        					objVertexTextureCoordBuffer[indexBlock] = [];
							objVertexNormal[indexBlock] = [];
        					objVertexIndexBuffer[indexBlock] = [];
        					blockBeginIndex = indexFace;
        				}
        				for(var j = 0; j < nbVertex; j++) {
        					var p = params[j + 1].split(/\//);

        					if(p[0] < 0) p[0] = listVertex.length+Number(p[0])+1;
        					if(p[1] < 0) p[1] = listTextureCoord.length+Number(p[1])+1;
							if(p.length > 2) {
								if(p[2] < 0) p[2] = listNormal.length+Number(p[2])+1;	
								this.pushVec3(objVertexNormal[indexBlock], listNormal[p[2] - 1]);
							}

        					this.pushVec3(objVertexPositionBuffer[indexBlock], listVertex[p[0] - 1]);
        					this.pushVec2(objVertexTextureCoordBuffer[indexBlock], listTextureCoord[p[1] - 1]);
        				}
        				for(var j = 0; j < nbVertex - 2; j++) {
        					objVertexIndexBuffer[indexBlock].push(indexFace - blockBeginIndex);
        					objVertexIndexBuffer[indexBlock].push(indexFace - blockBeginIndex + j + 1);
        					objVertexIndexBuffer[indexBlock].push(indexFace - blockBeginIndex + j + 2);
            			}
            			indexFace += nbVertex;
        			}
    			}
        	}
    	}
    }

    listObj.push({
		'name':groupName,
		'vPosition':objVertexPositionBuffer,
		'vTextureCoord':objVertexTextureCoordBuffer,
		'vNormal':objVertexNormal,
		'vIndex':objVertexIndexBuffer,
		'ready':false,
	});

    for(var i = 0; i < listObj.length; i++) {
    	var obj = listObj[i];
    	var part = new VertexGroup(this.gl, obj['name'], obj['vPosition'], obj['vTextureCoord'], obj['vNormal'], obj['vIndex']);
	    this.parts.push(part);
    	/*
    	for(var b = 0; b < obj['vIndex'].length; b++) {
	    	var part = new VertexArray(this.gl, obj['vPosition'][b], obj['vTextureCoord'][b], obj['vIndex'][b]);
	    	this.parts.push(part);
	    }*/
    }

    console.log(this.getGroupNames());
}

VertexObject.prototype.pushVec3 = function(b, vec3) {
	b.push(vec3[0]);
	b.push(vec3[1]);
	b.push(vec3[2]);
}

VertexObject.prototype.pushVec2 = function(b, vec2) {
	b.push(vec2[0]);
	b.push(vec2[1]);
}

VertexObject.prototype.getGroupNames = function() {
    var list = [];
    for(var i = 0; i < this.parts.length; i++) {
        list.push(this.parts[i].getName());
    }
    return list;
}

// draw the model
VertexObject.prototype.draw = function(pMatrix, mvMatrix, shader, texture) {

	for(var i = 0; i < this.parts.length; i++) {
    	this.parts[i].draw(pMatrix, mvMatrix, shader, texture);
    }

}
