/*
Creation date : 23/11/2017
Author : Sytrix
*/

function VertexGroup(gl, groupName, listPosition, listTextureCoord, listNormal, listIndex) {
	this.gl = gl;
	this.groupName = groupName;
	this.fragments = [];

	for(var i = 0; i < listIndex.length; i++) {
		var vertexArray = new VertexArray(gl, listPosition[i], listTextureCoord[i], listNormal[i], listIndex[i]); 
		this.fragments.push(vertexArray);
	}
}

VertexGroup.prototype.getName = function() {
	return this.groupName;
}

VertexGroup.prototype.draw = function(pMatrix, mvMatrix, shader, texture) {

	for(var i = 0; i < this.fragments.length; i++) {
		var fragment = this.fragments[i];
		fragment.draw(pMatrix, mvMatrix, shader, texture);
	}
}
