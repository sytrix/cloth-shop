/*
Creation date : 17/11/2017
Author : Sytrix
*/

function Viewer() {
	this.canvas = document.getElementById("canvas");

	try {
        this.gl = canvas.getContext("experimental-webgl");
        this.gl.viewportWidth = canvas.width;
        this.gl.viewportHeight = canvas.height;
        
        // TO DO loading

    } catch (e) {

    }
    if (!this.gl) {
    	alert('Could not initialise WebGL, sorry :-(');
    	return;
	}

	var shader = new Shader(this.gl, 'shader-vs', 'shader-fs');
	var texture = new Texture(this.gl, 'ressources/RUST_3d_Low1_Difuse.jpg');
	var vertexObject = new VertexObject(this.gl, 'ressources/RUST_3d_Low2.obj');
	//var texture = new Texture(this.gl, 'ressources/car.jpg');
	//var vertexObject = new VertexObject(this.gl, 'ressources/car.obj');

	this.model = new Model(this.gl);
	this.model.setShader(shader);
	this.model.setTexture(texture);
	this.model.setVertexObject(vertexObject);

	//this.text = new Text(this.gl, 'ressources/8x8-font.png');
	//this.shader = shader;

	this.lastTime = 0;

    this.gl.clearColor(0.5, 0.5, 0.5, 1.0);
    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.enable(this.gl.CULL_FACE);
    this.gl.cullFace(this.gl.BACK);  

    this.mvMatrix = mat4.create();
    this.mvMatrixStack = [];
    this.pMatrix = mat4.create();
    
    this.tick();
}

Viewer.prototype.tick = function() {
	var thisIsMe = this;
	window.requestAnimFrame(function() {thisIsMe.tick()});
    this.drawScene();
    this.animate();
}

Viewer.prototype.animate = function() {
    var timeNow = new Date().getTime();
    if (this.lastTime != 0) {
        var elapsed = timeNow - this.lastTime;
    }
    this.lastTime = timeNow;
}

Viewer.prototype.mvPushMatrix = function() {
	var copy = mat4.create();
	mat4.set(this.mvMatrix, copy);
	this.mvMatrixStack.push(copy);
}

Viewer.prototype.mvPopMatrix = function() {
    if (this.mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    this.mvMatrix = this.mvMatrixStack.pop();
}

Viewer.prototype.degToRad = function(degrees) {
    return degrees * Math.PI / 180;
}

Viewer.prototype.drawScene = function() {
	var gl = this.gl;
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, this.pMatrix);
    mat4.identity(this.mvMatrix);

    this.mvPushMatrix();
	mat4.translate(this.mvMatrix, [0.0, -0.7, -2.0]);        
	mat4.rotate(this.mvMatrix, (new Date).getTime() / 2000, [0, 1, 0]);
	mat4.scale(this.mvMatrix, [0.008, 0.008, 0.008]);
	//mat4.rotate(mvMatrix, (new Date).getTime() / 3000, [1, 0, 0]);
    //mat4.rotate(mvMatrix, degToRad(Math.sin((new Date).getTime() / 1000) * 20 + 10), [0, 0, 1]);
    //mat4.scale(mvMatrix, [5.0, 5.0, 5.0]);
    //mat4.rotate(this.mvMatrix, this.degToRad(Math.sin((new Date).getTime() / 1000) * 10 + 10), [1, 0, 0]);
	//mat4.rotate(this.mvMatrix, this.degToRad(Math.sin((new Date).getTime() / 1000) * 40), [0, 1, 0]);
    //mat4.rotate(this.mvMatrix, this.degToRad(Math.sin((new Date).getTime() / 1000) * 30), [0, 0, 1]);
   	
   	this.model.draw(this.pMatrix, this.mvMatrix);

    this.mvPopMatrix();

    //this.text.draw(this.pMatrix, this.mvMatrix, this.shader, [[0,0,0,0]], ['pierre']);
    /*
	mvPushMatrix();
    mat4.translate(mvMatrix, [0.0, 0.0, -30.0]);
    //mat4.rotate(mvMatrix, degToRad(rCube), [1, 1, 1]);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, neheTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexTexCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
    mat4.translate(mvMatrix, [oriCubeX, oriCubeY, oriCubeZ]);
    var m = mvMatrix;
    for(var i = 0; i < nbCubeZ; i++) {
    	for(var j = 0; j < nbCubeY; j++) {
    		for(var k = 0; k < nbCubeX; k++) {
		        if((i + j + k) % 2 == 0) {
		        	mvPushMatrix();
		        	mat4.translate(mvMatrix, [i * cubeSize, j * cubeSize, k * cubeSize]);
			        setMatrixUniforms();
			        gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
			        mvPopMatrix();
		        }
	        }
    	}
    }
    

    mvPopMatrix();*/

}
