/*
Creation date : 17/11/2017
Author : Sytrix
*/

function Texture(gl, url) {
	this.gl = gl;
	this.texture = gl.createTexture();
	this.texture.image = new Image();

	var thisIsMe = this;
	this.texture.image.onload = function() {
		thisIsMe.receive();
	}

	this.texture.image.src = url;
}

Texture.prototype.receive = function() {
	var gl = this.gl;
	gl.bindTexture(gl.TEXTURE_2D, this.texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.bindTexture(gl.TEXTURE_2D, null);
}

Texture.prototype.getTextureId = function() {
	return this.texture;
}